import scalafx.application.JFXApp
import scalafx.Includes._
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.event.ActionEvent
import scalafx.scene.text.Font
import scalafx.scene.layout.BorderPane
import scalafx.scene.layout.VBox
import scalafx.scene.layout.HBox

// function to manage the text that needs to be output
def combinedOutput(oldOutput:String,input:String):String = {
  //add the input string to the next line of the oldOutput string only if oldOutput only has 9 newlines or less
  val newOutput = if(oldOutput.split("\n").length >= 7){ // how to find the number of lines available? 21 magic number
    // stuff to do if there are already 9 lines of output (probably make a new string with the other 8 lines of output by splitting at 
    // \n to get an array of strings to form back together with the new string at the beginning)
    ""
  } else{
    // stuff to do if there are less than 9 lines of previous output
    //oldOutput+"\n"+input
    input+"\n"+oldOutput
  } 
  newOutput
}

def evaluateSimpleExpression(exp:String,lastIn:Double):String = {
  //"hurrr"// parse string to get numbers and operators, then evaluate it, and return it as a string
  val plus = "\\+"; val mult = "\\*"; val div = "\\/"; val sub = "-" // I am not typing a gorillion backslashes 
  val ex = if(exp.slice(exp.indexOf("a"),exp.indexOf("s")+1) == "ans"){
    exp.patch(exp.indexOf("a"),lastIn.toString,3) // 3 might have to change here if ans has issues
  } else exp
  val ans = if (ex.contains("+") == true && ex.split(plus).length == 2){
    val exArr = ex.split(plus)
    exArr(0).toDouble+exArr(1).toDouble
  } else if (ex.contains(sub) == true && ex.split(sub).length == 2){
    val exArr = ex.split(sub) 
    exArr(0).toDouble-exArr(1).toDouble
  } else if (ex.contains("*") == true && ex.split(mult).length == 2){
    val exArr = ex.split(mult)
    exArr(0).toDouble*exArr(1).toDouble
  } else if (ex.contains("/") == true && ex.split(div).length == 2){
    val exArr = ex.split(div)
    exArr(0).toDouble/exArr(1).toDouble
  } else {
    0.0
  }
  ans.toString
}

val app = new JFXApp {
	stage = new JFXApp.PrimaryStage {
		title = "NumKrunch"
		scene = new Scene(420, 360) {
          val biggerPane = new BorderPane
          val borderPane = new BorderPane
          val numberBar = new ToolBar
          val operationBar = new VBox
          val sendBar = new HBox
          val inputField = new TextField
          val outputArea = new TextArea
          val topLabel = new Label
          val sendButton = new Button("")
          val outputFont = new Font(20)
          var outputMsg = "Ready"
          outputArea.font = outputFont
          inputField.font = outputFont
          outputArea.promptText = ""
          outputArea.editable = false 
          sendButton.text = "Enter"
          sendButton.prefWidth = 100
          sendButton.prefHeight <== inputField.height
          inputField.prefWidth = 250
          sendBar.children = List(inputField,sendButton)
          topLabel.font = outputFont
          topLabel.text.value = outputMsg 
          
          borderPane.bottom = sendBar // TextField for input
          borderPane.center = outputArea// Text Area for output
          borderPane.top = topLabel
          biggerPane.center = borderPane // situate the serious parts of the calculator in the middle
          
          val numberButtons = for(i<- 0 to 9) yield {
            val button = new Button("")
            button.text = i.toString
            button.prefWidth = 35
            button.prefHeight = 60
            button.onAction = (b:ActionEvent) => {
              inputField.text = inputField.text.value+button.text.value
            }
            button
          }
          val opButtons = for(i<- 0 until 5) yield {
            val button = new Button("")
            val text = i match {
              case 0 => "+"
              case 1 => "-"
              case 2 => "*"
              case 3 => "/"
              case 4 => "ans"
              case _ => "oops"
            }  
            button.text = text
            button.prefWidth = 60
            button.prefHeight = 60
            button.onAction = (b:ActionEvent) => {
              inputField.text = inputField.text.value+button.text.value
            }
            button
          }
          numberBar.content = numberButtons
          biggerPane.bottom = numberBar
          operationBar.children = opButtons
          biggerPane.right = operationBar

          var lastInput = 0.0
          inputField.onAction = (input:ActionEvent) => { 
            val evaluated = evaluateSimpleExpression(inputField.text.value,lastInput)
            lastInput = evaluated.toDouble
            outputArea.text = combinedOutput(outputArea.text.value,evaluated)
            inputField.text = "" // clear input field
          }
          sendButton.onAction <== inputField.onAction 
          // THE MOMENT I REALIZED I COULD DO THIS WAS AMAZING!
          // GLORY TO SCALA AND LONG LIVE BINDINGS!
          root = biggerPane
		}
	}
}

app.main(args)
